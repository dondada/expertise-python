# coding: utf-8

from sqlalchemy import *
from sqlalchemy.pool import NullPool

stopProg = True # Booleen permettant de stoper le programme
stopQuestion = True # Booleen perettant de continuer ou non les modifications
stopID = True # Booleen permettant de boucler sur la question demandant l'id, boucle tant que l'id est incorrect
confirmation = True # Booleen permettant de continuer le programme ou non

engine = create_engine("mysql://root:user@password/simplytel", poolclass=NullPool) # Connexion à la BDD avec les identifiants

def requestChangeSIP(id, newName): # Fonction permettant d'update le nom
    with engine.connect() as connection:
        connection.execute(text("UPDATE Simply_clientsimply SET callID=:Name where id= :setID"), Name = newName, setID = id)

def showRequestSQL (): # Fonction permettant d'afficher les données de la BDD qui nous intéresse : ID et nom
    with engine.connect() as connection:
        result = connection.execute(text("select id,CallID from Simply_clientsimply"))
        for row in result:
            print "[ID :", row['id'], "]", "[CallID :", row['CallID'], "]"

showRequestSQL() # on affiche une première fois la table SQL

while stopProg:
    stopQuestion = True
    confirmation = True
    stopID = True
    while stopID: # Boucle demandant l'id, boucle tant que l'id est incorrect
        getID = raw_input("ID de la ligne SIP à changer : ")
        if not getID.isdigit(): # Condition permettant de vérifier si l'id est bien un chiffre
            print "l'ID ne peut pas comporter de lettre mais uniquement des chiffres !"
            stopID = True
        else:
            stopID = False
    newName = str(raw_input("Nouveau nom SIP à changer : ")) # on demande le nom à changer
    print "La ligne commportant l'ID =", getID , "sera changer en", newName # on affiche le futur résultat afin de vérifier que tout est bon
    while confirmation: # boucle
        getConfirmation = str(raw_input("Confirmez le changement ? [o / n] : "))
        if getConfirmation == "o":
            requestChangeSIP(getID, newName) # on effectu la requête SQL avec l'ID et le nouveau nom
            print "Le changement a bien été effectué. Voici la table SQL mis à jour :"
            showRequestSQL() # on affiche les tables SQL à jour
            confirmation = False
        elif getConfirmation == "n":
            print "Annulation du changement de nom !"
            confirmation = False
        else:
            print "Ce n'est pas une reponse valide ! Réponse attendu : o / n"
            confirmation = True
    while stopQuestion:
        wantContinu = str(raw_input("Voulez-vous continuer ? [o / n] : "))
        if wantContinu == "o":
            stopQuestion = False
        elif wantContinu == "n":
            print "Au revoir !"
            stopQuestion = False
            stopProg = False
        else:
            print "Ce n'est pas une reponse valide ! Réponse attendu : o / n"
            stopQuestion = True

            