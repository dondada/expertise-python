#!/usr/bin/env python
#encoding=utf-8

from random import *
import time

min_possible = int(input("minimum : "))
max_possible = int(input("maximum : "))
isValidIntervalle = max_possible - min_possible

if isValidIntervalle == 1:
    print "L'opération ne peut aboutir car l'intervalle doit être de 2 minimum."
    exit(0)

nombre_secret = randint(min_possible, max_possible) #génération du nombre aléatoire
proposition = (min_possible + max_possible) / 2  #Méthode de calcul pour trouver le nombre
nbCout = 0
old_proposition = 0 # variable stockant

print "\nLe nombre que l'IA doit trouver est :", nombre_secret, "\n"

if proposition == nombre_secret:
    print min_possible, "+", max_possible, "/ 2 =", proposition
    print proposition, ", L'algo a trouvé le nombre secret du premier coup!"

while proposition != nombre_secret:
    print min_possible, "+", max_possible, "/ 2 =", proposition
    if proposition > nombre_secret:
        print proposition, "est trop grand !\n"
        nbCout += 1
        max_possible = proposition
        proposition = (min_possible + max_possible) / 2
        time.sleep(1)
    elif proposition < nombre_secret:
        print proposition, "est trop petit !\n"
        nbCout += 1
        min_possible = proposition
        proposition = (min_possible + max_possible) / 2
        time.sleep(1)
    if nombre_secret == proposition:
        print min_possible, "+", max_possible, "/ 2 =", proposition
        print proposition, "\nL'algo a trouvé le nombre secret en", nbCout, "coups."
