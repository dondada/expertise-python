#!/usr/bin/env python3
#coding=utf-8

from random import *

nbSecret = randint(1,100)
nbCoup = 15
nbEssai = 1
etatDuJeu = True

while etatDuJeu:
    if nbCoup == 0:
        print ("Vous n'avez plus d'essais, le nombre secret n'a donc pas été trouvé.")
        etatDuJeu = False
    else:
        print ("Quel est le nombre secret ?")
        getProposition = int(input("Proposition : "))
        if getProposition > nbSecret:
            print ("C'est moins.")
            nbCoup -= 1
            nbEssai += 1
            etatDuJeu = True
        elif getProposition < nbSecret:
            print ("C'est plus.")
            nbCoup -= 1
            nbEssai += 1
            etatDuJeu = True
        else:
            print ("Bravo, le nombre secret a été trouvé en", nbEssai, "essais.")
            etatDuJeu = False