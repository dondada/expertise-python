#!/usr/bin/env python3
#coding=utf-8

# Initialisation des variables
nbNote = int(input("Nombre de note à saisir : "))
note = [] * nbNote
moyenneNote = 0
differenceNote = 0

# Initialisation du tableau
for i in range(nbNote):
    note.append(float(input("Saisir vos notes : ")))  # remplissage du tableau
print ("Voici les notes saisies :\n", note)

def moyenneDesNotes(note):
    moyenneNote = sum(note) / nbNote  # Calcul moyenne de la classe
    print ("\nLa moyenne est de", moyenneNote)

def ecartMinAndMax(note):
    noteMin = min(note)
    noteMax = max(note)
    differenceNote = noteMax - noteMin
    print ("La note minimale est", min(note))
    print ("La note maximale est", max(note))
    print ("L'écart entre la note minimum et la note maximum est de", differenceNote)

def nbNoteInferieurDix(note):
    nbNoteInferieurDix = 0
    for n in range(nbNote):
        if note[n] < 10:
            nbNoteInferieurDix += 1
    print ("Il y a", nbNoteInferieurDix, "notes en dessous de 10")

moyenneDesNotes(note)
ecartMinAndMax(note)
nbNoteInferieurDix(note)