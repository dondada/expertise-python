#!/usr/bin/env python
# coding=utf-8

age = int(input("Quel âge avez-vous ?\n"))

if age >= 18:
    print ("Vous êtes majeur.")
else:
    print ("Vous n'êtes pas majeur.")