#!/usr/bin/env python
# coding=utf-8

int_variable = 1
str_variable = "Bonjour"
float_variable = 3.14
bool_variable = True

print (type(int_variable))
print (type(str_variable))
print (type(float_variable))
print (type(bool_variable))
